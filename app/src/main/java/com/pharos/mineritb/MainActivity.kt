package com.pharos.mineritb

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pharos.mineritb.databinding.MainScreenBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: MainScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainScreenBinding.inflate(layoutInflater)

        setContentView(binding.root)
    }
}