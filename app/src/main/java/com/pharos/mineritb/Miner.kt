package com.pharos.mineritb

import android.annotation.SuppressLint
import android.graphics.Color
import android.widget.ImageView
import android.widget.TextView
import com.razerdp.widget.animatedpieview.AnimatedPieView
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig
import com.razerdp.widget.animatedpieview.data.SimplePieInfo
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

class Miner(
    maxOre: Int,
    private val time: Long,
    private val multiplier: TextView,
    private val count: TextView,
    private val pie: AnimatedPieView,
    private val ore: ImageView
) {
    private val channel = Channel<Int>(maxOre)

    fun start() = MainScope().launch {
        setClickListener()
        applyPieConfiguration()
        pie.start()
        delay(time)
        while (this.isActive) {
            channel.send(1)
            multiplier.inc()

            pie.start()

            delay(time)
        }
    }

    private fun setClickListener() {
        ore.setOnClickListener {
            runBlocking {
                channel.tryReceive().getOrNull()?.let {
                    multiplier .dec()
                    count.inc()
                }
            }
        }
    }

    private fun applyPieConfiguration() {
        val pieConfig = animatedMiningPieConfig
        pieConfig.duration(time)
        pieConfig.splitAngle(360F)
        pie.applyConfig(pieConfig)
    }

    @SuppressLint("ResourceType")
    private val animatedMiningPieConfig: AnimatedPieViewConfig =
        AnimatedPieViewConfig().addData(
            SimplePieInfo(
                1.0,
                Color.parseColor(pie.resources.getString(R.color.transparent_white)),
                "A"
            )
        )

    private fun TextView.inc() = this.editIntInText(1)

    private fun TextView.dec() = this.editIntInText(-1)

    private fun TextView.editIntInText(int: Int) {
        this.text.toString().let {
            this.text = if (it.first() == 'x') {
                "x" + (it.removeRange(0,1).toInt() + int)
            } else {
                it.toInt() + int
            }.toString()
        }
    }
}
