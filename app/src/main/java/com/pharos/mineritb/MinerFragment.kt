package com.pharos.mineritb

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.pharos.mineritb.databinding.FragmentMinerBinding


class MinerFragment : Fragment() {
    private lateinit var binding: FragmentMinerBinding
    private val maxOre = 3

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMinerBinding.inflate(layoutInflater)

        startMining()



        return binding.root
    }

    private fun startMining() {
        Miner(
            maxOre,
            1000,
            binding.multiplierIron,
            binding.countIron,
            binding.pieIron,
            binding.oreIron
        ).start()

        Miner(
            maxOre,
            2000,
            binding.multiplierCrystal,
            binding.countCrystal,
            binding.pieCrystal,
            binding.oreCrystal
        ).start()

        Miner(
            maxOre,
            3000,
            binding.multiplierDeuterium,
            binding.countDeuterium,
            binding.pieDeuterium,
            binding.oreDeuterium,
        ).start()
    }
}
